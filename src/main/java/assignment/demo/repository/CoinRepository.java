package assignment.demo.repository;

import assignment.demo.entity.Coin;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public interface CoinRepository extends JpaRepository<Coin, Long> {

    List<Coin> findByName(String name);

    List<Coin> findByMarketId(long id);
}
