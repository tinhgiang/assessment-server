package assignment.demo.repository;

import assignment.demo.entity.Market;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

@Service
public interface MarketRepository extends JpaRepository<Market, Long> {
}
