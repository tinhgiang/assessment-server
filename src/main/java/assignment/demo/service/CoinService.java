package assignment.demo.service;

import assignment.demo.entity.Coin;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public interface CoinService {

    Object store(Coin coin);

    List<Coin> listCoin();

    List<Coin> findCoinByName(String name);

    List<Coin> findCoinByMarketId(long id);
}
