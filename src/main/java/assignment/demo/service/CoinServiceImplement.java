package assignment.demo.service;

import assignment.demo.entity.Coin;
import assignment.demo.repository.CoinRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Calendar;
import java.util.List;

@Service
public class CoinServiceImplement implements CoinService {

    @Autowired
    CoinRepository coinRepository;

    @Override
    public Coin store(Coin coin) {
        try {
            if (coin == null) {
                return null;
            }
            coin.setStatus(Coin.Status.ACTIVE);
            coin.setCreatedAtMLS(Calendar.getInstance().getTimeInMillis());
            coin.setUpdatedAtMLS(Calendar.getInstance().getTimeInMillis());
            return (Coin) coinRepository.save(coin);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }

    @Override
    public List<Coin> listCoin() {
        return coinRepository.findAll();
    }

    @Override
    public List<Coin> findCoinByName(String name) {
        return coinRepository.findByName(name);
    }

    @Override
    public List<Coin> findCoinByMarketId(long id) {
        return coinRepository.findByMarketId(id);
    }
}
