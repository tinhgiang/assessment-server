package assignment.demo.service;

import assignment.demo.entity.Market;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface MarketService {

    Market store(Market market);

    List<Market> list();
}
